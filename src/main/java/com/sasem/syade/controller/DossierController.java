package com.sasem.syade.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sasem.syade.entities.Clientele;
import com.sasem.syade.entities.Declarant;
import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.Marchandises;
import com.sasem.syade.entities.SystemeHarmonise;
import com.sasem.syade.payload.DossierModel;
import com.sasem.syade.payload.EtapesDossier;
import com.sasem.syade.payload.MarchandisesDTO;
import com.sasem.syade.payload.ResponsePayload;
import com.sasem.syade.repository.ClienteleRepo;
import com.sasem.syade.repository.DeclarantRepo;
import com.sasem.syade.repository.DossierRepo;
import com.sasem.syade.repository.MarchandiseRepo;
import com.sasem.syade.repository.SHRepository;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api/v1/dossiers")
//@Api(description = "API pour les opérations CRUD sur les dossiers. Noter qu'on crée un dossier pour un client précis")
@Slf4j
public class DossierController {

	@Autowired
	private DossierRepo dossierService;

	@Autowired
	private ClienteleRepo clientService;

	@Autowired
	private SHRepository shRepo;

	@Autowired
	private MarchandiseRepo marchandiseRepo;

	@Autowired
	private DeclarantRepo declarantRepo;

	/*
	 * dossier-creer dossier-consulter dossier-supprimer dossier-modifier
	 * dossier-codifier_Marchandise dossier-attribuer
	 */

	@ApiOperation(value = "Creer un dossier attention il faut verifier que le client lui a bien été affecté")
	@RequestMapping(method = RequestMethod.POST)
	@Secured("dossierCreer")
	public Dossiers createDossier(@RequestBody DossierModel dossier) {
		Dossiers d = new Dossiers();
		if (dossier.getClientele() != null) {
			try {
				Clientele cl = clientService.findById(dossier.getClientele()).get();
				d.setCde(dossier.getCde());
				d.setCne(dossier.getCne());
				d.setDa(dossier.getDa());
				d.setDateBC(dossier.getDateBC());
				d.setDossier(dossier.getDossier());
				d.setLta(dossier.getLta());
				d.setNumFactureFournisseur(dossier.getNumFactureFournisseur());
				d.setDeclarationImportation(dossier.getDeclarationImportation());
				d.setNavireVol(dossier.getNavireVol());
				d.setNumFactureFret(dossier.getNumFactureFret());
				d.setNbrColis(dossier.getNbrColis());
				d.setNumAssurance(dossier.getNumAssurance());
				d.setNumCertificatSgs(dossier.getNumCertificatSgs());
				d.setCertificatOrigine(dossier.getCertificatOrigine());
				d.setCertificatDispense(dossier.getCertificatDispense());
				d.setCertificatPhytosanitaire(dossier.getCertificatPhytosanitaire());
				d.setAdresseLivraison(dossier.getAdresseLivraison());
				d.setAutreDocuments(dossier.getAutreDocuments());

				d.setMontantEXO(0);
				d.setMontantFac(0);
				d.setMontantHTBonC(0);
				d.setMontantTaxale(0);
				d.setMontantTSR(0);
				d.setMontantTTCBonC(0);
				d.setMontantTVA(0);
				d.setNetAPayerBonC(0);
				d.setNumeroBC("01"); // provisoire
				d.setObjetBC(dossier.getCne());
				d.setOt(dossier.getOt());
				d.setPoids(dossier.getPoids());
				d.setProformaOK("");
				d.setTauxChange(dossier.getTauxChange());
				d.setTauxTSR(0);
				d.setTypeFacture("");
				d.setValFobCfa(dossier.getValFobCfa());
				d.setValFob(dossier.getValFob());	
				d.setValCipCfa(dossier.getValCipCfa());	
				d.setMontantCip(dossier.getMontantCip());
				d.setMontantAssurance(dossier.getMontantAssurance());
				d.setMontantTransport(dossier.getMontantTransport());	
				d.setStatut(EtapesDossier.CREATION.name());
				d.setClientele(cl);
				d = this.dossierService.save(d);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("Dossier enregistré " + d);

		} else {
			log.info("le client n'a pas été renseigné Opération inachevé !!!");
		}
		return d;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	//@PreAuthorize("hasRole('ConsulterDossier')") Ceci ne marche pas: peut-être un pb de version Spring ou Java
	@Secured("ConsulterDossier")
	public List<Dossiers> getAllDossiers() {
		List<Dossiers> liste = new ArrayList<>();
		try {
			liste = this.dossierService.findAll();
			log.info("liste dossiers " + liste);
			Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return liste;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/client/{idClient}")
	//@PreAuthorize("hasRole('ConsulterDossier')")	
	@Secured("ConsulterDossier")
	public List<Dossiers> getDossiersByclient(@PathVariable(value = "idClient") Long id) {
		List<Dossiers> liste = new ArrayList<>();

		try {
			Clientele cl = clientService.findById(id).get();
			liste = this.dossierService.findByClientele(cl);
			Collections.reverse(liste);
			log.info("liste dossiers " + liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return liste;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	//@PreAuthorize("hasRole('SupprimerDossier')")
	@Secured("SupprimerDossier")
	public void deleteDossier(@PathVariable(value = "id") Long id) {
		try {
			if (id != null)
				this.dossierService.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	//@PreAuthorize("hasRole('ModifierDossier')")
	@Secured("ModifierDossier")
	public Dossiers updateDossier(@PathVariable(value = "id") Long id, @RequestBody DossierModel dossier) {

		Dossiers d = new Dossiers();
		try {
			d = this.dossierService.findById(id).get();
			d.setCde(dossier.getCde());
			d.setCne(dossier.getCne());
			d.setDa(dossier.getDa());
			d.setDateBC(dossier.getDateBC());
			d.setDetailsBC(dossier.getDetailsBC());
			d.setDossier(dossier.getDossier());
			d.setLta(dossier.getLta());
			d.setValFobCfa(dossier.getValFobCfa());
			d.setValFob(dossier.getValFob());	
			d.setValCipCfa(dossier.getValCipCfa());	
			d.setMontantCip(dossier.getMontantCip());
			d.setMontantAssurance(dossier.getMontantAssurance());
			d.setMontantTransport(dossier.getMontantTransport());	
			d.setNumeroBC(dossier.getNumeroBC());
			d.setObjetBC(dossier.getObjetBC());
			d.setOt(dossier.getOt());
			d.setPoids(dossier.getPoids());
			d.setTauxChange(dossier.getTauxChange());
			d.setTauxTSR(dossier.getTauxTSR());
			d.setValFob(dossier.getValFob());
			d.setNumFactureFournisseur(dossier.getNumFactureFournisseur());
			d.setDeclarationImportation(dossier.getDeclarationImportation());
			d.setNavireVol(dossier.getNavireVol());
			d.setNumFactureFret(dossier.getNumFactureFret());
			d.setNbrColis(dossier.getNbrColis());
			d.setNumAssurance(dossier.getNumAssurance());
			d.setNumCertificatSgs(dossier.getNumCertificatSgs());
			d.setCertificatOrigine(dossier.getCertificatOrigine());
			d.setCertificatDispense(dossier.getCertificatDispense());
			d.setCertificatPhytosanitaire(dossier.getCertificatPhytosanitaire());
			d.setAdresseLivraison(dossier.getAdresseLivraison());
			d.setAutreDocuments(dossier.getAutreDocuments());
			d = this.dossierService.save(d);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}

	@RequestMapping(value = "/dossier/{id}", method = RequestMethod.GET)
	@Secured("ConsulterDossier")
	public Dossiers dossierByID(@PathVariable(value = "id") Long id) {

		Dossiers d = new Dossiers();
		try {

			d = this.dossierService.findById(id).isPresent() ? this.dossierService.findById(id).get() : new Dossiers();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}

	@RequestMapping(value = "/dossierLibre", method = RequestMethod.GET)
	@Secured("ConsulterDossier")
	public List<Dossiers> dossierLibre() {
		List<Dossiers> liste = new ArrayList<>();

		try {
			liste = this.dossierService.getDossierLibre();
			log.info("liste dossiers " + liste);
			Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return liste;
	}

	@RequestMapping(value = "/dossierDeclarant/{idDeclarant}", method = RequestMethod.GET)
	@Secured("ConsulterDossier")
	public List<Dossiers> dossierDuDeclarant(@PathVariable(value = "idDeclarant") Long idDeclarant) {
		List<Dossiers> liste = new ArrayList<>();
		try {
			Declarant d = declarantRepo.findById(idDeclarant).get();
			liste = this.dossierService.findByDeclarant(d);
			Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return liste;
	}

	@GetMapping(value = "/listeSH")
	//@PreAuthorize("hasRole('dossierCodifier_Marchandise')")
	public List<SystemeHarmonise> listeSH() {
		return shRepo.findAll();
	}

	@GetMapping(value = "/listeSH/code/{code}")
	//@PreAuthorize("hasRole('dossierCodifier_Marchandise')")
	public List<SystemeHarmonise> getSHbyCode(@PathVariable(value = "code") String code) {
		return shRepo.findByHsCode(code);
	}

	@GetMapping(value = "/listeSH/allCode")
	//@PreAuthorize("hasRole('dossierCodifier_Marchandise')")
	public List<String> getSHAllCode() {
		return shRepo.findAllCode();
	}

	@GetMapping(value = "/listeSH/libelle/{lib}")
	//@PreAuthorize("hasRole('dossierCodifier_Marchandise')")
	public List<SystemeHarmonise> getSHbyLibelle(@PathVariable(value = "lib") String lib) {
		return shRepo.findByTardsc(lib);
	}

	@GetMapping(value = "/listeSH/allLibelle")
	//@PreAuthorize("hasRole('dossierCodifier_Marchandise')")
	public List<String> getSHAllLibelle() {
		System.out.println(shRepo.findAllLibelle());
		return shRepo.findAllLibelle();
	}

	@ApiOperation(value = "Ajouter des marchandises")
	@PostMapping(value = "/codifier/{idDossier}")	
	@Secured("dossierCodifierMarchandise")
	public ResponseEntity<ResponsePayload> addMarchandise(@PathVariable(value = "idDossier") long idDossier,
			@RequestBody List<MarchandisesDTO> liste) {

		try {
			log.info("==============   Enregistrement des marchandises **** Start =================");
			Dossiers d = dossierService.findById(idDossier).get();

			for (MarchandisesDTO m1 : liste) {
				Marchandises m = new Marchandises();
				if(m1.getIdMarchandise() != null) m.setIdMarchandise(m1.getIdMarchandise());
				m.setDossier(d);
				m.setFob(m1.getFob());
				m.setAssurance(m1.getAssurance());
				m.setPrixCip(m1.getPrixCip());
			    m.setHsCode(m1.getHsCode());
				m.setTransport(m1.getTransport());
				m.setLibelle(m1.getLibelle());
				m.setQuantite(m1.getQuantite());
				m.setPrixTotalFob(m1.getPrixTotalFob());
				m.setPrixTotalCip(m1.getPrixTotalCip());
				m.setPoids(m1.getPoids());
				marchandiseRepo.save(m);
			}
			//updateMontantDossier(d);
			//updateQuantiteColisDossier(d);
			d.setDateCodification(new Date());
			d.setStatut(EtapesDossier.CODIFICATION.name());
			dossierService.save(d);
			log.info("==============   Enregistrement des marchandises **** End =================");
			return ResponseEntity.accepted().body(new ResponsePayload(200, "Marchandises enregistrées"));

		} catch (Exception e) { // TODO Auto-generated catch block
			e.printStackTrace();
			log.info("==============   Enregistrement des marchandises  ****Error**** ================="
					+ e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePayload(500, "Marchandises non enregistrées"));

		}

	}
	
	private void updateQuantiteColisDossier(Dossiers d) {
		int nbrColis = 0;
		List<Marchandises> liste2 = new ArrayList<>();
		liste2 = marchandiseRepo.listerDossiers(d);
		for (Marchandises m : liste2) {
			nbrColis += m.getQuantite();
		}
		d.setNbrColis(nbrColis);
		dossierService.save(d);

	}

	private void updateMontantDossier(Dossiers d) {
		double montantFacture = 0.0;
		List<Marchandises> liste2 = new ArrayList<>();
		liste2 = marchandiseRepo.listerDossiers(d);
		for (Marchandises m : liste2) {
			montantFacture += m.getQuantite() * m.getFob();
		}
		d.setValFob(montantFacture);
		d.setValFobCfa(montantFacture*d.getTauxChange());
		dossierService.save(d);

	}

	@ApiOperation(value = "lister des marchandises")
	@GetMapping(value = "/marchandises/{idDossier}")
	@Secured("dossierCodifierMarchandise")
	public List<Marchandises> marchandiseDossier(@PathVariable(value = "idDossier") long idDossier) {

		try {
			log.info("==============   liste des marchandises d'un dossier**** Start =================");
			Dossiers d = dossierService.findById(idDossier).get();
			List<Marchandises> liste = marchandiseRepo.listerDossiers(d);
			Collections.reverse(liste);
			log.info("==============   Enregistrement des marchandises **** End =================");
			return liste;

		} catch (Exception e) { // TODO Auto-generated catch block
			e.printStackTrace();
			log.info("==============   Enregistrement des marchandises  ****Error**** ================="
					+ e.getMessage());
			return new ArrayList<>();

		}

	}

	@RequestMapping(value = "/marchandises/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ResponsePayload> deleteMarchandise(@PathVariable(value = "id") Long id) {
		try {
			Marchandises m = marchandiseRepo.findById(id).get();

			this.marchandiseRepo.deleteById(id);
			updateMontantDossier(m.getDossier());
			return ResponseEntity.accepted().body(new ResponsePayload(200, "marchandise supprimée"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.badRequest().body(new ResponsePayload(500, e.getMessage()));
		}
	}

	@ApiOperation(value = "Attribuer des dossiers a un déclarant")
	@Transactional
	@PostMapping(value = "/attribuerDossier/{idDeclarant}")
	@Secured("dossierAttribuer")
	public ResponseEntity<ResponsePayload> attribuerDossier(@PathVariable(value = "idDeclarant") long idDeclarant,
			@RequestBody List<Long> liste) {
		try {
			Declarant de = declarantRepo.findById(idDeclarant).get();
			List<Dossiers> l = dossierService.findByDeclarant(de);
			l.forEach(item -> {
				item.setDeclarant(null);
				dossierService.save(item);
			});
			liste.forEach(item -> {
				Dossiers d = dossierService.findById(item).get();
				d.setDeclarant(de);
				d.setStatut(EtapesDossier.ATTRIBUTION.name());
				d.setNomDeclarant(de.getNomEmploye()+" "+de.getPrenomEmploye());
				dossierService.save(d);
			});

			return ResponseEntity.accepted().body(new ResponsePayload(200, "Dossier attribué"));
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(new ResponsePayload(500, ex.getMessage()));
		}

	}

}
