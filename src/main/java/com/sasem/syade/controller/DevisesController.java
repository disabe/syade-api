package com.sasem.syade.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sasem.syade.entities.Clientele;
import com.sasem.syade.entities.Devises;
import com.sasem.syade.payload.ResponsePayload;
import com.sasem.syade.repository.DevisesRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/api/v1/devises")
@CrossOrigin(origins = "http://localhost:4200")
@Api( description="API pour les opérations sur les dévises.")
@Slf4j
public class DevisesController {

	

	@Autowired
	private DevisesRepo deviseService;

	/*
	 * creer dévise
	 */
	@ApiOperation(value = "Creer une dévise attention il faut verifier que la dévise du meme nom n'existe pas en bd")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ResponsePayload> createClientele(@RequestBody Devises de) {
		
		 try {
			this.deviseService.save(de);
			 log.info("devises créer ");
				
			 ResponsePayload response = new ResponsePayload();
			 response.setStatus(HttpStatus.OK.value());
			 response.setMessage("Dévise enregistrée avec succès");
			 return ResponseEntity.ok().body(response);
			
		} catch (Exception e) {
		
			 log.info("erreur création dévise "+ e.getMessage());
			 ResponsePayload response = new ResponsePayload();
			 response.setStatus(HttpStatus.BAD_REQUEST.value());
			 response.setMessage("erreur création dévise");
			 return ResponseEntity.badRequest().body(response);
		} 
		
	}
 
	@RequestMapping(method = RequestMethod.GET, value="/all")
	public List<Devises> getAllDevises() {
		List<Devises> liste = new ArrayList<>();
		
 	  	try {
			 liste = this.deviseService.findAll();
			 log.info("liste devises "+ liste);
			 Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	  	return liste;
	}
 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
	public void deleteClient(@PathVariable(value = "id") Long id) {
		try {
			this.deviseService.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Devises updateClient(@PathVariable(value = "id") int id, @RequestBody Devises de) {
		de.setIdDevise(id);
		Devises dev = new Devises(); 
		try {
			
			dev = this.deviseService.save(de);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dev;
	}
	
	
}
