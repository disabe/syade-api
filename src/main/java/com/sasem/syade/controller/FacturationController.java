package com.sasem.syade.controller;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.Facture;
import com.sasem.syade.entities.Service;
import com.sasem.syade.entities.ServiceAFacturer;
import com.sasem.syade.payload.EtapesDossier;
import com.sasem.syade.payload.ResponsePayload;
import com.sasem.syade.payload.ServiceAFacturerDTO;
import com.sasem.syade.repository.ClienteleRepo;
import com.sasem.syade.repository.DossierRepo;
import com.sasem.syade.repository.FactureRepo;
import com.sasem.syade.repository.ServiceFacturerRepo;
import com.sasem.syade.repository.ServiceRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@RestController
@RequestMapping(value = "/api/v1/facturation")
@CrossOrigin(origins = "http://localhost:4200")
@Api(description = "API pour les opérations autour de la facturation")
@Slf4j
public class FacturationController {
	@Autowired
	private ServiceRepo serviceRepo;
	@Autowired
	private DossierRepo dossierService;
	@Autowired
	private ServiceFacturerRepo ligneFacturerRepo;
	@Autowired
	private ClienteleRepo clientService;

	@Autowired
	private FactureRepo factureService;

	
	// SpringBootController## its work cool :)
	@GetMapping(value = "/facture/pdf/{idDossier}")
	public void missoutInward(@PathVariable(value = "idDossier") Long id, HttpServletResponse response) throws IOException {
		log.info("MISSOUT INWARD PDF REPORT :");
		//final InputStream stream = this.getClass().getResourceAsStream("/factures.jrxml");

		try {
					       
					JasperReport stream = JasperCompileManager.compileReport(this.getClass().getResourceAsStream("/factures.jrxml")); //master report
			        JasperReport subReport = JasperCompileManager.compileReport(this.getClass().getResourceAsStream("/serviceF.jrxml")); //sub report
			        JasperReport subReport1 = JasperCompileManager.compileReport(this.getClass().getResourceAsStream("/serviceF1.jrxml")); //sub report
			        Map<String, Object> parameters = new HashMap<>();
			        
			        parameters.put("subReport", subReport);
			        parameters.put("subReport1", subReport1);
			        parameters.put("idbon", id); 
			        
			            Class.forName("com.mysql.cj.jdbc.Driver");
			            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/llogisdb","root", "baccal");
			
			            final JasperPrint jasperPrint = JasperFillManager.fillReport(stream, parameters, con);
						response.setContentType("application/x-pdf");
						response.setHeader("Content-disposition", "inline; filename=App_report_en.pdf");

						final OutputStream outStream = response.getOutputStream();
						JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

			
			 con.close();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	

	public double calculComDebourFac(List<ServiceAFacturer> liste) {
		double debour = 0;
		double tva = 0;

		if (!liste.isEmpty()) {

			for (ServiceAFacturer m1 : liste) {
				if (m1.getCategorie().equals("Debours")) {
					debour += m1.getPrixTotal();
				}
			}

		}

		tva = Math.ceil(debour * 0.02);

		return tva;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/faturer/{idDossier}")
	public ResponseEntity<ResponsePayload> saveFacturerDossier(@PathVariable(value = "idDossier") Long id) {
		try{
			Dossiers newCMDD = dossierService.findById(id).get();
			List<ServiceAFacturer> listeServiceFacture = ligneFacturerRepo.findByIdDossier(newCMDD);
			if (newCMDD.getStatut().equals("cloture")) {

				dossierService.deleteById(id);

			}

			Service ser = new Service();
			ser = serviceRepo.findByLibelleServiceAndCategorie("COM. S/DEBOURS", "Debours");
			for (ServiceAFacturer se : listeServiceFacture) {
				if (ser.getLibelleService().equalsIgnoreCase(se.getService().getLibelleService())) {
					ligneFacturerRepo.delete(se);

				}

			}

			if (ser.getIdService() != null) {
				ServiceAFacturer newCmdVenduDs2 = new ServiceAFacturer();
				newCmdVenduDs2.setDateLivCMD(newCMDD.getDateBC());
				newCmdVenduDs2.setIdDossier(newCMDD);
				// double d = (double) Math.round(tonDouble * 100) / 100;
				newCmdVenduDs2.setQtteCMD(1);
				newCmdVenduDs2.setPrixUnitaire(calculComDebourFac(listeServiceFacture));
				newCmdVenduDs2.setService(ser);
				newCmdVenduDs2.setPrixTotal(newCmdVenduDs2.getPrixUnitaire());

				ligneFacturerRepo.save(newCmdVenduDs2);

			} else {
				ServiceAFacturer newCmdVenduDs2 = new ServiceAFacturer();
				Service newServ = new Service();

				newServ.setDateService(new Date());
				newServ.setCategorie("Debours");

				newServ.setLibelleService("COM. S/DEBOURS");
				newServ.setMateriel("");

				newServ.setPrixMax(calculComDebourFac(listeServiceFacture));
				newServ.setPrixMin(calculComDebourFac(listeServiceFacture));
				newServ.setReferenceServ("COM. S/DEBOURS".substring(0, 10).trim());
				newServ.setTauxReduction(0.0);
				newServ.setTauxTVA(19.25);
				serviceRepo.save(newServ);
				newCmdVenduDs2.setDateLivCMD(newCMDD.getDateBC());
				newCmdVenduDs2.setIdDossier(newCMDD);
				newCmdVenduDs2.setQtteCMD(1);

				newCmdVenduDs2.setPrixUnitaire(calculComDebourFac(listeServiceFacture));
				newCmdVenduDs2.setService(newServ);
				newCmdVenduDs2.setPrixTotal(newCmdVenduDs2.getPrixUnitaire());
				ligneFacturerRepo.save(newCmdVenduDs2);

			}

			List<ServiceAFacturer> listeCommande = new ArrayList<>();

			listeCommande = ligneFacturerRepo.findByIdDossier(newCMDD);

			for (int i = 0; i < listeCommande.size(); i++) {
				listeCommande.get(i).setFacture(1);
				ligneFacturerRepo.save(listeCommande.get(i));

			}
			Facture newFacure2 = new Facture();

			newFacure2.setIrFacture(0);
			newFacure2.setTvaFacture(newCMDD.getTvaBC());
			newFacure2.setTotalProduit(0);
			newFacure2.setTotalService(calculCommandeS(listeCommande));
			newFacure2.setMontantHTExonere(Math.ceil(calculCommandeExo(listeCommande)));
			newFacure2.setMontantHTTaxable(Math.ceil(calculCommandeTaxe(listeCommande)));
			newFacure2.setMontantFactHT(newFacure2.getMontantHTExonere() + newFacure2.getMontantHTTaxable());
			newFacure2.setMontantTVA(Math.ceil(calculTVA(newFacure2.getMontantHTTaxable(), newCMDD.getTvaBC())));
			newFacure2.setMontantIR(0.0);// calculIR(calculCommande(), selectedBonCMDFacture.getIrBC()));
			if (newCMDD.getMontantTSR() != 0) {
				newFacure2.setTauxTSR(newCMDD.getTauxTSR());
				newFacure2.setMontantTSR(newCMDD.getMontantTSR());
				newFacure2.setMontantFactureTTC(Math
						.ceil(newFacure2.getMontantFactHT() + newFacure2.getMontantTVA() + newFacure2.getMontantTSR()));

			} else {
				newFacure2.setTauxTSR(0.0);
				newFacure2.setMontantTSR(0.0);
				newFacure2.setMontantFactureTTC(Math.ceil(newFacure2.getMontantFactHT() + newFacure2.getMontantTVA()));
			}

			newFacure2.setNetAPfacture(newFacure2.getMontantFactureTTC());// calculCommande() -
																			// newFacure.getMontantIR());
			newFacure2.setDateFacture(new Date());

			newFacure2.setObjetFacture(newCMDD.getObjetBC());
			newFacure2.setMontantEnLettres(
					factureLettre(Double.valueOf(newFacure2.getNetAPfacture()).longValue()).toUpperCase() + " F. CFA");
			newFacure2.setIdDossier(newCMDD);
			newFacure2.setTypeFacture(newCMDD.getTypeFacture());
			newFacure2.setSolde(newFacure2.getNetAPfacture());
			newCMDD.setStatut(EtapesDossier.FACTURATION.name());
			factureService.save(newFacure2);
			dossierService.save(newCMDD);
		} catch (Exception ex) {
			log.info(ex.getMessage());
		}

		return null;
	}

	public static final String french =
			// the main rule set
			"%main:\n"
					// negative-number and fraction rules
					+ "    -x: moins >>;\n" + "    x.x: << virgule >>;\n"
					// words for numbers from 0 to 10
					+ "    z\u00e9ro; un; deux; trois; quatre; cinq; six; sept; huit; neuf;\n"
					+ "    dix; onze; douze; treize; quatorze; quinze; seize;\n"
					+ "        dix-sept; dix-huit; dix-neuf;\n"
					// ords for the multiples of 10: %%alt-ones inserts "et"
					// when needed
					+ "    20: vingt[->%%alt-ones>];\n" + "    30: trente[->%%alt-ones>];\n"
					+ "    40: quarante[->%%alt-ones>];\n" + "    50: cinquante[->%%alt-ones>];\n"
					// rule for 60. The /20 causes this rule's multiplier to be
					// 20 rather than 10, allowinhg us to recurse for all values
					// from 60 to 79...
					+ "    60/20: soixante[->%%alt-ones>];\n"
					// ...except for 71, which must be special-cased
					+ "    71: soixante et onze;\n"
					// at 72, we have to repeat the rule for 60 to get us to 79
					+ "    72/20: soixante->%%alt-ones>;\n"
					// at 80, we state a new rule with the phrase for 80. Since
					// it changes form when there's a ones digit, we need a second
					// rule at 81. This rule also includes "/20," allowing it to
					// be used correctly for all values up to 99
					+ "    80: quatre-vingts; 81/20: quatre-vingt->>;\n"
					// "cent" becomes plural when preceded by a multiplier, and
					// the multiplier is omitted from the singular form
					+ "    100: cent[ >>];\n" + "    200: << cents;\n" + "    201: << cent[ >>];\n"
					+ "    300: << cents;\n" + "    301: << cent[ >>];\n" + "    400: << cents;\n"
					+ "    401: << cent[ >>];\n" + "    500: << cents;\n" + "    501: << cent[ >>];\n"
					+ "    600: << cents;\n" + "    601: << cent[ >>];\n" + "    700: << cents;\n"
					+ "    701: << cent[ >>];\n" + "    800: << cents;\n" + "    801: << cent[ >>];\n"
					+ "    900: << cents;\n" + "    901: << cent[ >>];\n" + "    1000: mille[ >>];\n"
					// values from 1,100 to 1,199 are rendered as "onze cents..."
					// instead of "mille cent..." The > after "1000" decreases
					// the rule's exponent, causing its multiplier to be 100 instead
					// of 1,000. This prevents us from getting "onze cents cent
					// vingt-deux" ("eleven hundred one hundred twenty-two").
					+ "    1100>: onze cents[ >>];\n"
					// at 1,200, we go back to formating in thousands, so we
					// repeat the rule for 1,000
					+ "    1200: mille >>;\n"
					// at 2,000, the multiplier is added
					+ "    2000: << mille[ >>];\n" + "    1,000,000: << million[ >>];\n"
					+ "    2,000,000: << millions[ >>];\n" + "    1,000,000,000: << milliard[ >>];\n"
					+ "    2,000,000,000: << milliards[ >>];\n" + "    1,000,000,000,000: << billion[ >>];\n"
					+ "    2,000,000,000,000: << billions[ >>];\n" + "    1,000,000,000,000,000: =#,##0=;\n"
					// %%alt-ones is used to insert "et" when the ones digit is 1
					+ "%%alt-ones:\n" + "    ; et-un; =%main=;";

	public String factureLettre(Long montant) {
		// double num = 2718.28;
		String enLettre = "";
		/**
		 * Spellout rules for French. French adds some interesting quirks of its own: 1)
		 * The word "et" is interposed between the tens and ones digits, but only if the
		 * ones digit if 1: 20 is "vingt," and 2 is "vingt-deux," but 21 is
		 * "vingt-et-un." 2) There are no words for 70, 80, or 90. "quatre-vingts"
		 * ("four twenties") is used for 80, and values proceed by score from 60 to 99
		 * (e.g., 73 is "soixante-treize" ["sixty-thirteen"]). Numbers from 1,100 to
		 * 1,199 are rendered as hundreds rather than thousands: 1,100 is "onze cents"
		 * ("eleven hundred"), rather than "mille cent" ("one thousand one hundred")
		 */

		NumberFormat formatter = new RuleBasedNumberFormat(french, Locale.FRANCE);
		// NumberFormat formatter = new RuleBasedNumberFormat(Locale.FRANCE,
		// RuleBasedNumberFormat.SPELLOUT);
		enLettre = formatter.format(montant);
		// System.out.println(enLettre);
		return enLettre;
		// résultat avec Locale.ENGLISH : two thousand seven hundred and eighteen point
		// two eight
		// résultat avec Locale.GERMAN : zwei tausend sieben hundert achtzehn Komma zwei
		// acht
		// résultat avec Locale.FRANCE : deux-mille-sept-cent-dix-huit virgule deux huit
	}

	public Double calculTVA(Double montant, Double tva) {
		Double montantTVA = (montant * tva) / 100;
		return montantTVA;
	}

	

	public Double calculCommandeS(List<ServiceAFacturer> liste) {
		
		Double montantHTs = 0.0;
		for (int i = 0; i <= liste.size() - 1; ++i) {

			montantHTs = montantHTs
					+ (liste.get(i).getQtteCMD() * liste.get(i).getPrixUnitaire());

		}
		return montantHTs;
	}

	public Double calculCommandeExo(List<ServiceAFacturer> liste) {
		
		Double montantHTs = 0.0;
		for (int i = 0; i <= liste.size() - 1; i++) {
			if (liste.get(i).getService().getCategorie().equals("Debours")){
			if (liste.get(i).getService().getLibelleService()
					.equalsIgnoreCase("TAXE PASSAGE MAGASIN")) {

				if (liste.get(i).getIdDossier().getClientele()
						.getMinMagasin() > (liste.get(i).getQtteCMD()
								* liste.get(i).getPrixUnitaire())) {
					montantHTs = montantHTs + liste.get(i).getIdDossier().getClientele().getMinMagasin();
				} else {
					montantHTs = montantHTs + (liste.get(i).getQtteCMD()
							* liste.get(i).getPrixUnitaire());
				}

			} else {
				montantHTs = montantHTs
						+ (liste.get(i).getQtteCMD() * liste.get(i).getPrixUnitaire());

			}
			}
		}
		return montantHTs;
	}

	public Double calculCommandeTaxe(List<ServiceAFacturer> liste) {
		
		Double montantHTs = 0.0;
		for (int i = 0; i <= liste.size() - 1; ++i) {
			if (liste.get(i).getService().getCategorie().equals("Prestations")){
			if (liste.get(i).getService().getLibelleService().equalsIgnoreCase("MANUTENTION")) {

				if (liste.get(i).getIdDossier().getClientele()
						.getMinManutention() > (liste.get(i).getQtteCMD()
								* liste.get(i).getPrixUnitaire())) {
					montantHTs = montantHTs + liste.get(i).getIdDossier().getClientele().getMinManutention();
				} else {
					montantHTs = montantHTs + (liste.get(i).getQtteCMD()
							* liste.get(i).getPrixUnitaire());
				}

			} else {
				montantHTs = montantHTs + (liste.get(i).getQtteCMD()
						* liste.get(i).getPrixUnitaire());
			}
		}
		}
		return montantHTs;
	}
	

	@RequestMapping(method = RequestMethod.GET, value = "/servicesDossier/{idDossier}")
	public List<ServiceAFacturer> getServicesAFacturerByDossier(@PathVariable(value = "idDossier") Long id) {

		List<ServiceAFacturer> liste = new ArrayList<>();

		try {
			Dossiers d = dossierService.findById(id).get();
			liste = this.ligneFacturerRepo.findByIdDossier(d);
			log.info("liste service " + liste);
			Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return liste;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteServiceDossier(@PathVariable(value = "id") Long id) {
		try {
			this.ligneFacturerRepo.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * creer service
	 */
	@ApiOperation(value = "enregistrer les elements de la facture")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ResponsePayload> enregVenteService(@RequestBody List<ServiceAFacturerDTO> liste) {
		List<ServiceAFacturer> liste1 = new ArrayList<>();

		try {
			Dossiers d = dossierService.findById(liste.get(0).getIdDossier()).get();
			liste1 = this.ligneFacturerRepo.findByIdDossier(d);
			if (!liste1.isEmpty()) {
				ligneFacturerRepo.deleteAll(liste1);
			}
			liste.forEach(se -> {
				log.info("element facture ", se.getIdService(), se.getPrixUnitaire());
				Service s = serviceRepo.findById(se.getIdService()).get();

				if (s.getCategorie().equals("Debours")) {
					ServiceAFacturer sf = new ServiceAFacturer();
					sf.setQtteCMD(1);

					Dossiers newCMDD = new Dossiers();
					newCMDD = dossierService.findById(se.getIdDossier()).get();
					Double poids = newCMDD.getPoids();
					log.info("element dossier " + newCMDD.getIdDossier() + "  " + newCMDD.getLta());
					sf.setService(s);
					sf.setLibelleService(s.getLibelleService());
					sf.setCategorie(s.getCategorie());

					if (s.getLibelleService().contains("A D C")) {

						double total = 0;
						sf.setPrixUnitaire(newCMDD.getClientele().getTauxADC());
						total = sf.getPrixUnitaire() * poids;

						sf.setPrixTotal(total);
						sf.setQtteCMD(poids.intValue());

					} else if (s.getLibelleService().equalsIgnoreCase("TAXE PASSAGE MAGASIN")) {

						double total = 0;
						sf.setPrixUnitaire(newCMDD.getClientele().getTauxMagasin());
						total = sf.getPrixUnitaire() * poids;
//                           newLignePros.setQte(selectedProforma.getPoids());

						if (total > newCMDD.getClientele().getMinMagasin()) {
							sf.setPrixTotal(total);
							sf.setQtteCMD(poids.intValue());
						} else {
							sf.setPrixTotal(newCMDD.getClientele().getMinMagasin());
							sf.setQtteCMD(poids.intValue());
						}
					} else if (s.getLibelleService().equalsIgnoreCase("Frais de banque")) {
						sf.setPrixUnitaire(se.getPrixUnitaire() + (se.getPrixUnitaire() * 0.1925));
						sf.setPrixTotal(sf.getPrixUnitaire() * sf.getQtteCMD());
						sf.setTvaExterne(sf.getPrixUnitaire() * 0.1925);
					} else {
						sf.setPrixUnitaire(se.getPrixUnitaire());
						sf.setPrixTotal(se.getPrixUnitaire() * sf.getQtteCMD());
					}

					sf.setIdDossier(newCMDD);
					sf.setTauxTVA(newCMDD.getTvaBC());
					sf.setMontantTVA((sf.getTauxTVA() / 100) * sf.getPrixTotal());
					sf.setMontantTotalTtc(sf.getPrixTotal() + sf.getMontantTVA());

					/*
					 * Service newServ1 = new Service(); newServ1 =
					 * serviceRepo.findById(s.getIdService()).get(); if
					 * (newServ1.getLibelleService().equalsIgnoreCase("FRET")) { Service ser = new
					 * Service(); ser =
					 * serviceRepo.findByLibelleServiceAndCategorie("FRAIS RETOURS DE FONDS",
					 * "Debours"); if (s.getIdService() != null) { ServiceAFacturer newLigne = new
					 * ServiceAFacturer(); newLigne.setDateLivCMD(new Timestamp(0));
					 * newLigne.setIdDossier(newCMDD); // double d = (double) Math.round(tonDouble *
					 * 100) / 100; newLigne.setQtteCMD(1);
					 * newLigne.setPrixUnitaire(Math.ceil((sf.getPrixUnitaire() * 2) / 100));
					 * newLigne.setService(ser); newLigne.setPrixTotal(newLigne.getPrixUnitaire());
					 * ligneFacturerRepo.save(newLigne);
					 * 
					 * } else { Service newServ = new Service(); newServ.setDateService(new
					 * Timestamp(0)); newServ.setCategorie("Debours");
					 * newServ.setLibelleService("FRAIS RETOURS DE FONDS"); newServ.setMateriel("");
					 * newServ.setPrixMax(Math.ceil((sf.getPrixUnitaire() * 2) / 100));
					 * newServ.setPrixMin(Math.ceil((sf.getPrixUnitaire() * 2) / 100));
					 * newServ.setReferenceServ("FRAIS RETOURS DE FONDS".substring(0, 10).trim());
					 * newServ.setTauxReduction(0.0); newServ.setTauxTVA(19.25);
					 * serviceRepo.save(newServ);
					 * 
					 * ServiceAFacturer newLigne = new ServiceAFacturer();
					 * newLigne.setDateLivCMD(new Timestamp(0)); newLigne.setIdDossier(newCMDD);
					 * newLigne.setQtteCMD(1);
					 * 
					 * newLigne.setPrixUnitaire(Math.ceil((sf.getPrixUnitaire() * 2) / 100));
					 * newLigne.setService(newServ);
					 * newLigne.setPrixTotal(newLigne.getPrixUnitaire());
					 * ligneFacturerRepo.save(newLigne);
					 * 
					 * } newCMDD = dossierService.findById(newCMDD.getIdDossier()).get();
					 * newCMDD.setTauxTSR(15.0); newCMDD.setMontantTSR((sf.getPrixUnitaire() *
					 * newCMDD.getTauxTSR()) / 100); dossierService.save(newCMDD); }
					 */

					ligneFacturerRepo.save(sf);
					log.info("element facture enregistré ");
				} else {
					ServiceAFacturer sf = new ServiceAFacturer();
					sf.setQtteCMD(1);

					Dossiers newCMDD = new Dossiers();
					newCMDD = dossierService.findById(se.getIdDossier()).get();
					Double poids = newCMDD.getPoids();

					sf.setService(s);
					sf.setLibelleService(s.getLibelleService());
					sf.setCategorie(s.getCategorie());
					log.info("element service " + s.getLibelleService() + "  " + s.getCategorie());
					if (!s.getLibelleService().equalsIgnoreCase("COM. S/DEBOURS")) {

						if (s.getLibelleService().equalsIgnoreCase("Manutention")) {

							double total = 0;
							sf.setPrixUnitaire(newCMDD.getClientele().getTauxManutention());
							total = sf.getPrixUnitaire() * poids;

							if (total > newCMDD.getClientele().getMinManutention()) {
								sf.setPrixTotal(total);
								sf.setQtteCMD(poids.intValue());
							} else {
								sf.setPrixTotal(newCMDD.getClientele().getMinManutention());
								sf.setQtteCMD(poids.intValue());
							}

						} else if (s.getLibelleService().equalsIgnoreCase("Transport")) {
							if (newCMDD.getClientele().getMinKiloTransport() < newCMDD.getPoids()) {
								sf.setPrixTotal(newCMDD.getPoids() * newCMDD.getClientele().getTauxTransport());
								sf.setPrixUnitaire(newCMDD.getClientele().getTauxTransport());
								sf.setQtteCMD(poids.intValue());
							} else {
								sf.setPrixUnitaire(sf.getPrixUnitaire());
								sf.setPrixTotal(sf.getPrixUnitaire() * sf.getQtteCMD());

							}
						}

						// gestion des HAD
						else if (s.getLibelleService().concat("123456789").subSequence(0, 9).equals("H A D -15")) {

							if (newCMDD.getValCipCfa() < 1000000.0) {
								sf.setPrixTotal(50000.0);
								sf.setPrixUnitaire(50000.0);
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 1000000.0) && (newCMDD.getValCipCfa() < 2000000.0)) {
								sf.setPrixTotal(Math.ceil(80000.0 - (80000.0 * 0.15)));
								sf.setPrixUnitaire(Math.ceil(80000.0 - (80000.0 * 0.15)));
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 2000000.0) && (newCMDD.getValCipCfa() < 6000000.0)) {
								sf.setPrixTotal(Math.ceil(163000.0 - (163000.0 * 0.15)));
								sf.setPrixUnitaire(Math.ceil(163000.0 - (163000.0 * 0.15)));
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 6000000.0) && (newCMDD.getValCipCfa() < 10000000.0)) {
								sf.setPrixTotal(Math.ceil(240000.0 - (240000.0 * 0.15)));
								sf.setPrixUnitaire(Math.ceil(240000.0 - (240000.0 * 0.15)));
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 10000000.0)) {
								sf.setPrixTotal(Math.ceil(0.005 * newCMDD.getValCipCfa() + 245000.0
										- ((0.005 * newCMDD.getValCipCfa() + 245000.0) * 0.15)));
								sf.setPrixUnitaire(Math.ceil(0.005 * newCMDD.getValCipCfa() + 245000.0
										- ((0.005 * newCMDD.getValCipCfa() + 245000.0) * 0.15)));
								sf.setQtteCMD(1);
							}

						}

						else if (s.getLibelleService().equals("H A D")) {

							if (newCMDD.getValCipCfa() < 1000000.0) {
								sf.setPrixTotal(50000.0);
								sf.setPrixUnitaire(50000.0);
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 1000000.0) && (newCMDD.getValCipCfa() < 2000000.0)) {
								sf.setPrixTotal(80000.0);
								sf.setPrixUnitaire(80000.0);
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 2000000.0) && (newCMDD.getValCipCfa() < 6000000.0)) {
								sf.setPrixTotal(163000.0);
								sf.setPrixUnitaire(163000.0);
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 6000000.0) && (newCMDD.getValCipCfa() < 10000000.0)) {
								sf.setPrixTotal(240000.0);
								sf.setPrixUnitaire(240000.0);
								sf.setQtteCMD(1);
							}

							else if ((newCMDD.getValCipCfa() >= 10000000.0)) {
								sf.setPrixTotal(Math.ceil(0.005 * newCMDD.getValCipCfa() + 245000.0));
								sf.setPrixUnitaire(Math.ceil(0.005 * newCMDD.getValCipCfa() + 245000.0));
								sf.setQtteCMD(1);
							}

						}

						else {
							sf.setPrixUnitaire(se.getPrixUnitaire());
							sf.setPrixTotal(sf.getPrixUnitaire() * sf.getQtteCMD());
						}

						sf.setIdDossier(newCMDD);
						sf.setTauxTVA(newCMDD.getTvaBC());

						sf.setMontantTVA((sf.getTauxTVA() / 100) * sf.getPrixTotal());
						sf.setMontantTotalTtc(sf.getPrixTotal() + sf.getMontantTVA());

						try {
							newCMDD.setStatut(EtapesDossier.PILOTAGE.name());
							dossierService.save(newCMDD);
							ligneFacturerRepo.save(sf);
							log.info("element facture enregistré ");

						} catch (Exception ex) {

						}

					}

				}

			});

			ResponsePayload response = new ResponsePayload();
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("éléments enregistrés avec succès");
			return ResponseEntity.ok().body(response);
		} catch (Exception ex) {
			log.info("erreur création client " + ex.getMessage());
			ResponsePayload response = new ResponsePayload();
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			response.setMessage("erreur enregistrements elements facture");
			return ResponseEntity.badRequest().body(response);
		}

	}

}
