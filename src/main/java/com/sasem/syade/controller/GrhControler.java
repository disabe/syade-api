package com.sasem.syade.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sasem.syade.entities.Clientele;
import com.sasem.syade.entities.Declarant;
import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.Role;
import com.sasem.syade.payload.DeclarantDTO;
import com.sasem.syade.payload.DeclarantDTO2;
import com.sasem.syade.payload.ResponsePayload;
import com.sasem.syade.repository.ClienteleRepo;
import com.sasem.syade.repository.DeclarantRepo;
import com.sasem.syade.repository.DossierRepo;
import com.sasem.syade.repository.MarchandiseRepo;
import com.sasem.syade.repository.RoleRepo;
import com.sasem.syade.repository.SHRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/api/v1/grh")
@CrossOrigin(origins = "http://localhost:4200")
@Api( description="API pour les opérations sur les employées.")
@Slf4j
public class GrhControler {
	
	@Autowired
	private DossierRepo dossierService;

	@Autowired
	private DeclarantRepo declarantRepo;
	
	@Autowired
	private RoleRepo roleRepo;
	
	@Autowired
	PasswordEncoder encoder;
	
	/*
	 * declarant-creer declarant-consulter declarant-supprimer declarant-modifier
	 */	
	    @ApiOperation(value = "Creer un déclarant")
		@RequestMapping(value = "/addDeclarant",  method = RequestMethod.POST)
	    //@PreAuthorize("hasRole('declarant-creer')")
		public ResponseEntity<ResponsePayload> createDeclarant(@RequestBody DeclarantDTO dec) {
		 Declarant de = new Declarant();
		 Declarant de1 = new Declarant();
			 try {
		
				 de.setAdresse(dec.getAdresse());
				 de.setDateNaissance(new Timestamp(0));
				 de.setEmailEmp(dec.getEmailEmp());
				 de.setMatricule(dec.getMatricule());
				 de.setNomEmploye(dec.getNomEmploye());
				 de.setPrenomEmploye(dec.getPrenomEmploye());
				 de.setQualite(dec.getQualite());
				 de.setTelephone(dec.getTelephone());
				de1 = this.declarantRepo.save(de);
				return ResponseEntity.accepted().body(new ResponsePayload(200, "enregistrement effectuée"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 log.info("erreur création client "+ e.getMessage());
				 return ResponseEntity.badRequest().body(new ResponsePayload(500, "erreur survenue"));
			} 
			
		}
	     
	    @ApiOperation(value = "liste des utilisateurs")
	    @RequestMapping(method = RequestMethod.GET, value = "/all")
	    //@PreAuthorize("hasRole('declarant-consulter')")
		public List<Declarant> getAllDeclarant() {
			List<Declarant> liste = new ArrayList<>();

			try {
				liste = this.declarantRepo.findAll();
				log.info("liste dossiers " + liste);
				 Collections.reverse(liste);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return liste;
		}
	    
	    @ApiOperation(value = "liste des roles")
	    @RequestMapping(method = RequestMethod.GET, value = "/allRole")
	    //@PreAuthorize("hasRole('declarant-consulter')")
		public List<Role> getAllRoles() {
			List<Role> liste = new ArrayList<>();

			try {
				liste = this.roleRepo.findAll();
				log.info("liste roles " + liste);				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return liste;
		}
	    @ApiOperation(value = "liste des roles du déclarant")
	    @RequestMapping(method = RequestMethod.GET, value = "/allRoleDeclarant/{id}")
	    //@PreAuthorize("hasRole('declarant-consulter')")
		public Set<Role> getAllRolesDeclarant(@PathVariable(value = "id") Long id) {
			Set<Role> liste = new HashSet<>();

			try {
				Declarant d = declarantRepo.findById(id).get();
				liste = d.getRoles();
				log.info("liste roles " + liste);				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return liste;
		}
	    
	    
	    @ApiOperation(value = "un utilisateur")
	    @RequestMapping(method = RequestMethod.GET, value = "/declarant/{id}")
	   // @PreAuthorize("hasRole('declarant-consulter')")
		public Declarant getOneDeclarant(@PathVariable(value = "id") Long id) {
			Declarant dec = new Declarant();

			try {
				dec = this.declarantRepo.findById(id).get();
				log.info("liste dossiers " + dec);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return dec;
		}
	    
	    @RequestMapping(value = "declarant/name/{nom}", method = RequestMethod.GET)
	    //@PreAuthorize("hasRole('declarant-consulter')")
		public List<Declarant> getDeclarantByName(@PathVariable(value = "nom") String nom) {
	    	try {
	   		return this.declarantRepo.findByNomEmploye(nom);
	    	} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	 	
		@RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE) 
		 //@PreAuthorize("hasRole('declarant-supprimer')")
		public ResponseEntity<ResponsePayload> deleteDeclarant(@PathVariable(value = "id") Long id) {
			
			try {
				
				this.declarantRepo.deleteById(id);
				return ResponseEntity.accepted().body(new ResponsePayload(200, "Suppression effectuée"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return ResponseEntity.badRequest().body(new ResponsePayload(500, "Erreur survenue"));
			}
		}
		
		@RequestMapping(value = "update/{id}", method = RequestMethod.PUT)
		// @PreAuthorize("hasRole('declarant-modifier')")
		public ResponseEntity<ResponsePayload> updateDeclarant(@PathVariable(value = "id") Long id, @RequestBody DeclarantDTO de) {
			Declarant Empl = new Declarant();
			Empl.setIdEmploye(id);
			Empl.setDateNaissance(new Timestamp(0));
			Empl.setAdresse(de.getAdresse());
			Empl.setEmailEmp(de.getEmailEmp());
			Empl.setEnabled(true);
			Empl.setMatricule(de.getMatricule());
			Empl.setNomEmploye(de.getNomEmploye());
			Empl.setPassword(encoder.encode(de.getPassword()));
			Empl.setPrenomEmploye(de.getPrenomEmploye());
			Empl.setQualite(de.getQualite());
			Empl.setTelephone(de.getTelephone());
			Empl.setUsername(de.getUsername());
			Set<Role> setRoles = new HashSet<>();
	 		try {
	 			de.getRoles().forEach(item ->{
	 				Role r = roleRepo.findByLibelleRole(item);
	 				setRoles.add(r);
	 			});
	 			Empl.setRoles(setRoles);
	 			Empl = this.declarantRepo.save(Empl);
				return ResponseEntity.accepted().body(new ResponsePayload(200, "Mise à jour effectuée"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return ResponseEntity.badRequest().body(new ResponsePayload(500, "Erreur survenue"));
			}
	 		
	 		
	 	}
		
		
		
	    
	

}
