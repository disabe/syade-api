package com.sasem.syade.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sasem.syade.entities.Declarant;
import com.sasem.syade.entities.Role;
import com.sasem.syade.payload.DeclarantDTO;
import com.sasem.syade.payload.DeclarantDTO2;
import com.sasem.syade.repository.DeclarantRepo;
import com.sasem.syade.repository.RoleRepo;
import com.sasem.syade.security.JwtResponse;
import com.sasem.syade.security.LoginRequest;
import com.sasem.syade.security.MessageResponse;
import com.sasem.syade.services.UserDetailsImpl;
import com.sasem.syade.services.UserDetailsServiceImpl;
import com.sasem.syade.utils.AuthEntryPointJwt;
import com.sasem.syade.utils.AuthTokenFilter;
import com.sasem.syade.utils.JwtUtils;


import lombok.Builder;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	DeclarantRepo userRepository;

	@Autowired
	RoleRepo roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	
	  @Autowired
	  private UserDetailsServiceImpl userDetailsService;
	
	@RequestMapping(value = "/signin",  method = RequestMethod.POST, //
	 produces = { MediaType.APPLICATION_JSON_VALUE, //
   MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) { 

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
//if(authentication.isAuthenticated()) {
		
		/*
		 * final UserDetails userDetails =
		 * userDetailsService.loadUserByUsername(loginRequest.getUsername());
		 * 
		 * final String token = jwtUtils.generateToken(userDetails);
		 */
		
		
		  SecurityContextHolder.getContext().setAuthentication(authentication); String
		  jwt = jwtUtils.generateJwtToken(authentication);
		 
		
		
		  UserDetailsImpl userDetails = (UserDetailsImpl)
		  authentication.getPrincipal(); List<String> roles =
		  userDetails.getAuthorities().stream() .map(item -> item.getAuthority())
		  .collect(Collectors.toList());
		  
		  return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(),
		  userDetails.getUsername(), userDetails.getEmail(), roles));
		 
	
		
		/*
		 * }else { return ResponseEntity.badRequest().body(null); }
		 */
	}

	@GetMapping("/signup")
	public ResponseEntity<?> registerUser() {
		//@RequestBody DeclarantDTO2 signUpRequest
		/*
		 * if (userRepository.existsByUsername(signUpRequest.getUsername())) { return
		 * ResponseEntity .badRequest() .body(new
		 * MessageResponse("Error: Username is already taken!")); }
		 * 
		 * if (userRepository.existsByEmailEmp(signUpRequest.getEmailEmp())) { return
		 * ResponseEntity .badRequest() .body(new
		 * MessageResponse("Error: Email is already in use!")); }
		 * 
		 * if (!signUpRequest.getPassword().equals(signUpRequest.getPasswordRepeat())) {
		 * return ResponseEntity .badRequest() .body(new
		 * MessageResponse("Error: Password must equals to password repeat!")); }
		 */
		

		// Create new user's account
		//Declarant user = userRepository.findById(signUpRequest.getId()).get();
		Declarant user = userRepository.findById(7209L).get();
		user.setUsername("didier");
		user.setPassword( encoder.encode("didier"));
		
		 List<Role> liste2 = new ArrayList<>(); liste2 = roleRepository.findAll();				  
		  Set<Role> liste3 = new HashSet<>(); liste3.addAll(liste2);
		  user.setRoles(liste3);
		  		
		
		/*
		 * Set<String> strRoles = signUpRequest.getRoles(); Set<Role> roles = new
		 * HashSet<>(); strRoles.stream().forEach(item ->{ Role r =
		 * roleRepository.findByLibelleRole(item); roles.add(r); });
		 */
		
		if(user.getRoles().isEmpty()) {

		/*if (strRoles == null) {
			Groups userRole = roleRepository.findByLibelleGroup(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Groups adminRole = roleRepository.findByLibelleGroup(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Groups modRole = roleRepository.findByLibelleGroup(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Groups userRole = roleRepository.findByLibelleGroup(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}
		
		*/
		}
		
		//user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
	
	
	
}
