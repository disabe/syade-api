package com.sasem.syade.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.Resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.sasem.syade.entities.Clientele;
import com.sasem.syade.payload.ResponsePayload;
import com.sasem.syade.services.ClientService;
import com.sasem.syade.repository.ClienteleRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/api/v1/clients")
@CrossOrigin(origins = "http://localhost:4200")
@Api( description="API pour les opérations sur les clients.")
@Slf4j
public class ClientController {

	

	@Autowired
	private ClienteleRepo clientService;

	/*
	 * client-creer client-consulter client-supprimer client-modifier
	 */
	 @ApiOperation(value = "Creer un client attention il faut verifier que le type d'operation pour le meme client n'existe pas en bd")
	@RequestMapping(method = RequestMethod.POST)
	 //@PreAuthorize("hasRole('client-creer') or hasRole('MODERATOR') or hasRole('ADMIN')")
	 //@PreAuthorize("hasRole('client-creer')")
	public ResponseEntity<ResponsePayload> createClientele(@RequestBody Clientele client) {
		 Clientele cl = new Clientele();
		 try {
			cl = this.clientService.save(client);
			 log.info("client créer "+ cl);				
			 ResponsePayload response = new ResponsePayload();
			 response.setStatus(HttpStatus.OK.value());
			 response.setMessage("Client enregistré avec succès");
			 return ResponseEntity.ok().body(response);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			 log.info("erreur création client "+ e.getMessage());
			 ResponsePayload response = new ResponsePayload();
			 response.setStatus(HttpStatus.BAD_REQUEST.value());
			 response.setMessage("erreur création client");
			 return ResponseEntity.badRequest().body(response);
		} 
		
	}
 
	@RequestMapping(method = RequestMethod.GET, value="/all")
	 //@PreAuthorize("hasRole('client-consulter')")
	public List<Clientele> getAllClients() {
		List<Clientele> liste = new ArrayList<>();
		
 	  	try {
			 liste = this.clientService.findAll();
			 log.info("liste clients "+ liste);
			 Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	  	return liste;
	}
 
	@RequestMapping(value = "byname/{raisonSociale}", method = RequestMethod.GET)
	 //@PreAuthorize("hasRole('client-consulter')")
	public List<Clientele> getClientShortName(@PathVariable(value = "raisonSociale") String raisonSociale) {
		 //Collections.reverse(liste);
   		return this.clientService.findByRaisonSocialeClient(raisonSociale);
   		
	}
 	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
	 //@PreAuthorize("hasRole('client-consulter')")
	public void deleteClient(@PathVariable(value = "id") Long id) {
		try {
			this.clientService.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	// @PreAuthorize("hasRole('client-modifier')")
	public Clientele updateClient(@PathVariable(value = "id") Long id, @RequestBody Clientele client) {
		client.setIdClient(id);
 Clientele cl = new Clientele();
 		try {
			cl = this.clientService.save(client);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		return cl;
 	}
	
	@RequestMapping(value = "/{raison}", method = RequestMethod.GET)
	// @PreAuthorize("hasRole('client-consulter')")
	public List<Clientele> clientByRaisonSocial(@PathVariable(value = "raison") String raison) {
		
 //Clientele cl = new Clientele();
		List<Clientele> cl = new ArrayList<>(); 
 		try {
			cl = this.clientService.findByRaisonSocialeClient(raison);
			 Collections.reverse(cl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
 		return cl;
 	}
	
	@RequestMapping(value = "/client/{id}", method = RequestMethod.GET)
	public Clientele clientByID(@PathVariable(value = "id") Long id) {
		
    Clientele cl = new Clientele();
    
 		try{
 			
			cl = this.clientService.findById(id).isPresent() ? this.clientService.findById(id).get() : new Clientele();
		
 		}
 		catch(Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		return cl;
 	}
	
}
