package com.sasem.syade.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class JWTClientExample {
	
	static final String URL_CLIENTS = "http://localhost:8888/api/v1/clients/all";
    static final String URL_EMPLOYEES = "http://localhost:8888/api/v1/grh/all";
    static final String URL_DOSSIERS= "http://localhost:8888/api/v1/dossiers/all";
 
    private static void callRESTApi(String url) {
        // HttpHeaders
        HttpHeaders headers = new HttpHeaders();
       final String authorizationString = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkaWRpZXIiLCJpYXQiOjE2ODk1MjY3NzksImV4cCI6MTY4OTYxMzE3OX0.1khwxmP4yY5fvR6hVRqP5qJqnLU3YzsZfIA2NHOIq1lFv3LNmVYoc5SFpnKKdN2kom_bRAvcS_NmMGkkGhcdRg";
        //
        // Authorization string (JWT)
        //
        headers.set("Authorization", authorizationString);
        //
        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
  
        // Request to return JSON format
        headers.setContentType(MediaType.APPLICATION_JSON);
  
        // HttpEntity<String>: To get result as String.
        HttpEntity<String> entity = new HttpEntity<String>(headers);
  
        // RestTemplate
        RestTemplate restTemplate = new RestTemplate();
  
        // Send request with GET method, and Headers.
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
  
        String result = response.getBody();
  
        System.out.println(result);
    }
  
	
	  public static void main(String[] args) { 
		  String username = "didier"; 
		  String password = "didier";
	  
	 /* String authorizationString = postLogin(username, password);
	  
	  System.out.println("Authorization String=" + authorizationString);*/
	  
	   //Call REST API: 
		   callRESTApi(URL_CLIENTS); 
		   callRESTApi(URL_EMPLOYEES); 
		   callRESTApi(URL_DOSSIERS);
	  }
	 
    
    
    
    
    
    
    
    
    
    
    
    
    
}
