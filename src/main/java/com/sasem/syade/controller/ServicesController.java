package com.sasem.syade.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sasem.syade.entities.Clientele;
import com.sasem.syade.entities.Devises;
import com.sasem.syade.entities.Service;
import com.sasem.syade.payload.ResponsePayload;
import com.sasem.syade.payload.ServiceDTO;
import com.sasem.syade.repository.ServiceRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "/api/v1/services")
@CrossOrigin(origins = "http://localhost:4200")
@Api( description="API pour le crud sur les services.")
@Slf4j
public class ServicesController {

	@Autowired
	private ServiceRepo serviceRepo;
	
	/*
	 * creer service
	 */
	@ApiOperation(value = "Creer un service attention il faut verifier que le service du meme nom n'existe pas en bd")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Service> createClientele(@RequestBody ServiceDTO se) {
		 
		 try {
			 Service s = new Service();
			 Service s1 = new Service();
			 s.setCategorie(se.getCategorie());
			 s.setDateService(new Timestamp(0));
			 s.setLibelleService(se.getLibelleService());
			 s.setPrixMax(se.getPrixMax());
			s1 = this.serviceRepo.save(s);
			 log.info("service créer ");
				
				/*
				 * ResponsePayload response = new ResponsePayload();
				 * response.setStatus(HttpStatus.OK.value());
				 * response.setMessage("Service enregistrée avec succès");
				 */
			 return ResponseEntity.ok().body(s1);
			
		} catch (Exception e) {
		
			 log.info("erreur création service "+ e.getMessage());
			 ResponsePayload response = new ResponsePayload();
			 response.setStatus(HttpStatus.BAD_REQUEST.value());
			 response.setMessage("erreur création service");
			 return ResponseEntity.badRequest().body(null);
		} 
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value="/all")
	public List<Service> getAllServices() {
		List<Service> liste = new ArrayList<>();
		
 	  	try {
			 liste = this.serviceRepo.findAll();
			 log.info("liste service "+ liste);
			 Collections.reverse(liste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	  	return liste;
	}
 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
	public void deleteService(@PathVariable(value = "id") Long id) {
		try {
			this.serviceRepo.deleteById(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Service updateService(@PathVariable(value = "id") long id, @RequestBody Service de) {
		de.setIdService(id);
		Service dev = new Service(); 
		try {
			
			dev = this.serviceRepo.save(de);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dev;
	}
	
	
	
	
	
	
	
}
