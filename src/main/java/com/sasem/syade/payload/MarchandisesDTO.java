package com.sasem.syade.payload;

import java.io.Serializable;

import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
public class MarchandisesDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idMarchandise;
	private String libelle;	
	private double quantite;
	private String hsCode;	
	private Double prixTotalFob;
	private Double prixTotalCip;
	private Double prixCip;
	private Double transport;
	private Double fob;
	private Double assurance;
	private Double poids;
	private long idDossier;
	
}
