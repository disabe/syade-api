package com.sasem.syade.payload;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;

import lombok.Data;

@Data
public class DeclarantDTO2 {
	private Long id;	
	private String emailEmp;	
	private String username;
	private String password;
	private String passwordRepeat;
	private Set<String> roles; 
	
}
