package com.sasem.syade.payload;

import java.sql.Timestamp;

import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.Service;
import com.sasem.syade.entities.ServiceAFacturer;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServiceAFacturerDTO {

	private Long idLignCMD;
	private Timestamp dateLivCMD;
	//private Timestamp dateRetrait;
	//private byte etatCMD;	
	private double montantHT;
	private double montantTVA;
	private String libelleService;
	private String categorie;

	//@Column(length=255)
	//private String observations;
	//private double prixMax;
	private double prixTotal;
	private double prixUnitaire;
	private int qtteCMD;
	//private double tauxReduc;
	private double tauxTVA;
	private double tvaExterne;	
	private long idService;
	private long idDossier;

}
