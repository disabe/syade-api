package com.sasem.syade.payload;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;

import lombok.Data;

@Data
public class DeclarantDTO {
	private String adresse;	
	private String matricule;	
	private String nomEmploye;	
	private String prenomEmploye;
	private String emailEmp;
	private String qualite;
	private String telephone;
	private String username;
	private String password;
	private Set<String> roles; 
	
}
