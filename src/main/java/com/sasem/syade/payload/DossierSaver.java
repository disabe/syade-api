package com.sasem.syade.payload;

import java.util.Date;

import javax.persistence.Column;

import lombok.Data;

@Data
public class DossierSaver {

	private Long clientele;
	private String cde;
	private double tauxChange;
	private double poids;
	private String ot;

	private String lta;

	private String dossier;
	private Double montantAssurance;
	private Double montantTransport;
	private String statut;
	private String da;
	private Double valCipCfa;
	private Date dateBC;
	private Double valFobCfa;
	private String cne;
	private double valFob;
	private Double montantCip;
	private String numFactureFournisseur;
	private String declarationImportation;
	private String navireVol;
	private String numFactureFret;
	private Integer nbrColis;
	private String numAssurance;
	private String numCertificatSgs;
	private String certificatOrigine;
	private String certificatDispense;
	private String certificatPhytosanitaire;
	private String adresseLivraison;
	private String autreDocuments;

}
