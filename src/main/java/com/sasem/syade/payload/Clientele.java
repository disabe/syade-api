package com.sasem.syade.payload;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Timestamp;


/**
 * The persistent class for the clientele database table.
 * 
 */

@Data
public class Clientele implements Serializable {
	
	private Long idClient;
	
	private String adresseClient;	
	
	private String bp;	
	
	private Timestamp dateEnreg;
	
	private String emailClient;

	private String faxClient;
	
	private String matriculeClient;

	private double minKiloTransport;

	private double minMagasin;

	private double minManutention;
	
	private String codeIban;
	
	private String numeroContribuable;
	
	private String numeroRegCon;
	
	private String raisonSocialeClient;
	
	private String siteWebClient;

	private double tauxADC;

	private double tauxMagasin;

	private double tauxManutention;

	private double tauxTransport;

	private String telClient;

	private String typeOperation;


}