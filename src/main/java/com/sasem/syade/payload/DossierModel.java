package com.sasem.syade.payload;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Date;
import java.math.BigInteger;

@Data
public class DossierModel implements Serializable {

	private Long idDossier;
	private String cde;
	private String cne;
	private String da;
	private Date dateBC;
	private Date datecloture;
	private String statut;
	private String detailsBC;
	private String dossier;
	private String nomDeclarant;
	private double irBC;
	private String lta;
	private Double valCipCfa;
	private Double valFobCfa;
	private double valFob;
	private Double montantCip;
	private double montantEXO;
	private Double montantAssurance;
	private Double montantTransport;
	private double montantFac;
	private double montantHTBonC;
	private double montantIR;
	private double montantTaxale;
	private double montantTSR;
	private double montantTTCBonC;
	private double montantTVA;
	private double netAPayerBonC;
	private String numeroBC;
	private String objetBC;
	private String ot;
	private double poids;
	private String proformaOK;
	private double tauxChange;
	private double tauxTSR;
	private double tvaBC;
	private String typeFacture;
	private Long clientele;
	private Long declarant;
	private String numFactureFournisseur;
	private String declarationImportation;
	private String navireVol;
	private String numFactureFret;
	private Integer nbrColis;
	private String numAssurance;
	private String numCertificatSgs;
	private String certificatOrigine;
	private String certificatDispense;
	private String certificatPhytosanitaire;
	private String adresseLivraison;
	private String autreDocuments;

}