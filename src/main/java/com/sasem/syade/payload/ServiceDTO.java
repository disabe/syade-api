package com.sasem.syade.payload;

import java.sql.Timestamp;

import javax.persistence.Column;

import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.Service;
import com.sasem.syade.entities.ServiceAFacturer;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServiceDTO {

	private String categorie;

	
	private String libelleService;	
	

	private double prixMax;

	
}
