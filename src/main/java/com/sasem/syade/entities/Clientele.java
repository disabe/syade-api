package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the clientele database table.
 * 
 */
@Entity
@Table(name="clientele")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Clientele implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idClient", unique=true, nullable=false)
	private Long idClient;

	@Column(name="adresseClient", length=255)
	private String adresseClient;

	
	@Column(name="bp", length=50)
	private String bp;
	
	@Column(name="dateEnreg")
	private Timestamp dateEnreg;

	@Column(name="emailClient", length=50)
	private String emailClient;

	@Column(name="fax_client", length=20)
	private String faxClient;

	@Column(name="matriculeClient", length=100)
	private String matriculeClient;

	private double minKiloTransport;

	private double minMagasin;

	private double minManutention;

	@Column(name="code_iban", length=100)
	private String codeIban;

	@Column(name="numero_contribuable", length=50)
	private String numeroContribuable;

	@Column(name="numero_reg_con", length=50)
	private String numeroRegCon;

	@Column(name="raisonSocialeClient", length=100)
	private String raisonSocialeClient;

	@Column(name="siteWebClient", length=255)
	private String siteWebClient;

	private double tauxADC;

	private double tauxMagasin;

	private double tauxManutention;

	private double tauxTransport;

	@Column(name="tel_client", length=20)
	private String telClient;

	@Column(name="type_operation", length=20)
	private String typeOperation;

	@JsonIgnore
	//bi-directional many-to-one association to Dossier
	@OneToMany(mappedBy="clientele", fetch=FetchType.EAGER)
	private List<Dossiers> dossiers;



}