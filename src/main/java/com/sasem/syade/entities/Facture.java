package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the factures database table.
 * 
 */
@Entity
@Table(name="factures")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Facture implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idFacture;

	@Column(name = "cde", length=255)
	private String cde;

	@Column(name = "cne", length=255)
	private String cne;

	@Column(name = "da", length=255)
	private String da;
	
	@Column(name = "date_facture")
	private Date dateFacture;

	@Lob
	@Column(name = "details_facture", length = 255)
	private String detailsFacture;

	@Column(name = "dossier", length=50)
	private String dossier;	
	
	@Column(name = "ir_facture")
	private double irFacture;

	@Column(name = "lta", length=255)
	private String lta;

	@Lob
	@Column(name = "montant_en_lettres", length=255)
	private String montantEnLettres;
	
	@Column(name = "montant_factht")
	private double montantFactHT;
	
	@Column(name = "montant_facturettc")
	private double montantFactureTTC;

	@Column(name = "montanthtexonere")
	private double montantHTExonere;

	@Column(name = "montanthttaxable")
	private double montantHTTaxable;

	@Column(name = "montantir")
	private double montantIR;

	@Column(name = "montanttsr")
	private double montantTSR;

	@Column(name = "montanttva")
	private double montantTVA;

	@Column(name = "netapfacture")
	private double netAPfacture;

	@Column(name = "num_facture", length=50)
	private String numFacture;

	@Column(name = "objet_facture", length=255)
	private String objetFacture;

	@Column(name = "ot", length=20)
	private String ot;
	
	@Column(name = "solde")
	private double solde;
	
	@Column(name = "tauxtsr")
	private double tauxTSR;
	
	@Column(name = "total_produit")
	private double totalProduit;
	
	@Column(name = "total_service")
	private double totalService;

	@Column(name = "tva_facture")
	private double tvaFacture;

	@Column(name = "type_facture", length=50)
	private String typeFacture;
	
	@Column(name = "val")
	private double val;

	@Column(name = "version")
	private int version;
	
	@OneToOne(cascade=CascadeType.ALL)//one-to-one
    @JoinColumn(name="idDossier")
	private Dossiers idDossier;

	
}