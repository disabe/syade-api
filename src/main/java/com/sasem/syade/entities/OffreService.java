package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the offre_service database table.
 * 
 */
@Entity
@Table(name="offre_service")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class OffreService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idOffre;

	@Lob
	private byte[] contenuOffre;

	@Temporal(TemporalType.TIMESTAMP)
	private Date datetime;

	private BigInteger idProspect;

	@Column(length=255)
	private String libelleOffre;

	@Column(length=255)
	private String objetOffre;

	@Column(name="pieces_jointes", length=255)
	private String piecesJointes;

	

}