package com.sasem.syade.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="marchandises")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Marchandises implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idMarchandise", unique=true, nullable=false)
	private Long idMarchandise;	
	@Column(name="libelle", length=1024)
	private String libelle;	
	@Column(name="quantite")
	private Double quantite;	
	@Column(name="prix_total_fob")
	private Double prixTotalFob;
	@Column(name="prix_total_cip")
	private Double prixTotalCip;
	@Column(name="hsCode", length=1024)
	private String hsCode;	
	@Column(name="prix_cip")
	private Double prixCip;
	@Column(name="fob")
	private Double fob;
	@Column(name="transport")
	private Double transport;
	@Column(name="assurance")
	private Double assurance;
	@Column(name="poids")
	private Double poids;
	@JsonIgnore
	@ManyToOne()
	@JoinColumn(name="idDossier")
	private Dossiers dossier;
	
}
