package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.math.BigInteger;


/**
 * The persistent class for the facturesgroupedebours database table.
 * 
 */
@Entity
@Table(name="facturesgroupedebours")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Facturesgroupedebour implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idFacture;

	@Column(length=20)
	private String cde;

	@Column(length=20)
	private String cne;

	@Column(length=20)
	private String da;

	private Timestamp dateFacture;

	@Lob
	private String detailsFacture;

	@Column(length=50)
	private String dossier;

	private double fraisBanque;

	private double guce;

	@OneToOne(cascade=CascadeType.ALL)//one-to-one
    @JoinColumn(name="idDossier")
	private Dossiers idDossier;


	@Column(length=50)
	private String lta;

	@Lob
	private String montantEnLettres;

	private double montantFactHT;

	private double montantFactureTTC;

	private double montantHTExonere;

	private double montantHTTaxable;

	private double montantTSR;

	private double montantTVA;

	private double netAPfacture;

	@Column(length=50)
	private String numFacture;

	@Column(length=255)
	private String objetFacture;

	@Column(length=20)
	private String ot;

	private double solde;

	private double tauxTSR;

	private double totalService;

	private double tvaBanque;

	private double tvaFacture;

	private double val;

	private int version;

	

}