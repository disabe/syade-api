package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.math.BigInteger;


/**
 * The persistent class for the dossiers database table.
 * 
 */
@Entity
@Table(name="dossiers")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Dossiers implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idDossier;
	@Column(name="cde", length=20)
	private String cde;
	@Column(name="cne", length=50)
	private String cne;
	@Column(name="da", length=20)
	private String da;
	@Column(name="datebc")
	private Date dateBC;	
	@Column(name="datecloture")
	private Date datecloture;	
	@Column(name="statut", length=255)
	private String statut;	
	@Lob
	@Column(name="detailsbc")
	private String detailsBC;
	@Column(name="dossier", length=50)
	private String dossier;	
	@Column(name="lta", length=50)
	private String lta;
	@Column(name="montantexo")
	private double montantEXO;
	@Column(name="montant_fac")
	private double montantFac;
	@Column(name="montanthtbonc")
	private double montantHTBonC;	
	@Column(name="montantir")
	private double montantIR;
	@Column(name="montant_taxable")
	private double montantTaxale;
	@Column(name="montanttsr")
	private double montantTSR;
	@Column(name="montantttcbonc")
	private double montantTTCBonC;
	@Column(name="montanttva")
	private double montantTVA;
	@Column(name="netapayer_bonc")
	private double netAPayerBonC;
	@Column(name="numerobc", length=50)
	private String numeroBC;
	@Column(name="objetbc", length=255)
	private String objetBC;
	@Column(name="ot", length=20)
	private String ot;
	@Column(name="poids")
	private double poids;
	@Column(name="proformaok", length=50)
	private String proformaOK;
	@Column(name="taux_change")
	private double tauxChange;
	@Column(name="tauxtsr")
	private double tauxTSR;
	@Column(name="tvabc")
	private double tvaBC;
	@Column(name="type_facture", length=50)
	private String typeFacture;	
	@Column(name="val_fob")
	private double valFob;	
	@Column(name="val_fob_cfa")
	private Double valFobCfa;
	@Column(name="val_cip_cfa")
	private Double valCipCfa;
	@Column(name="montant_cip")
	private Double montantCip;
	@Column(name="montant_assuarance")
	private Double montantAssurance;
	@Column(name="montant_transport")
	private Double montantTransport;
	@Column(name="num_facture_fournisseur", length=255)
	private String numFactureFournisseur;	
	@Column(name="declaration_importation", length=255)
	private String declarationImportation;
	@Column(name="navire_vol", length=255)
	private String navireVol;
	@Column(name="num_facture_fret", length=255)
	private String numFactureFret;
	@Column(name="nbr_colis")
	private Integer nbrColis;
	@Column(name="num_assurance", length=255)
	private String numAssurance;
	@Column(name="num_certificat_sgs", length=255)
	private String numCertificatSgs;
	@Column(name="certificat_origine", length=255)
	private String certificatOrigine;
	@Column(name="certificat_dispense", length=255)
	private String certificatDispense;
	@Column(name="certificat_phytosanitaire", length=255)
	private String certificatPhytosanitaire;
	@Column(name="adresse_livraison", length=255)
	private String adresseLivraison;
	@Column(name="autre_documents", length=1024)
	private String autreDocuments;	
	@Column(name="nom_declarant", length=255)
	private String nomDeclarant;	
	@Column(name="numero_camcis", length=255)
	private String numeroCamcis;	
	@Column(name="date_camcis")
	private Date dateCamcis;	
	@Column(name="date_attribution")
	private Date dateAttribution;	
	@Column(name="date_codification")
	private Date dateCodification;
	@Column(name="date_facturation")
	private Date dateFacturation;	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="idClient")
	private Clientele clientele;	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_declarant")
	private Declarant declarant;
	@JsonIgnore
	@OneToMany(mappedBy="dossier")
	private List<Marchandises> marchandises;	
	@JsonIgnore
	@OneToMany(mappedBy="idDossier")
	private List<ServiceAFacturer> listeLigneServices;


}