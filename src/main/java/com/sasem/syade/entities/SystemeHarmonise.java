package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * The persistent class for the systeme_harmonise database table.
 * 
 */
@Entity
@Table(name="systeme_harmonise")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SystemeHarmonise implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_sh", unique=true, nullable=false)
	private Long idSh;

	@Column(name="code_mercurial", length=255)
	private String codeMercurial;

	@Column(length=50)
	private String dac;

	@Column(length=50)
	private String ddi;

	@Column(length=50)
	private String dds;

	@Column(name="hs_code", length=255)
	private String hsCode;

	@Column(length=50)
	private String ise;

	@Column(length=50)
	private String isi;

	@Column(length=50)
	private String ste;

	@Column(length=1024)
	private String tardsc;

	@Column(length=50)
	private String tcc;

	@Column(length=50)
	private String tcv;

	@Column(length=50)
	private String tva;

	@Column(length=50)
	private String vpf;

	

}