package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;


/**
 * The persistent class for the typedepense database table.
 * 
 */
@Entity
@Table(name="typedepense")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Typedepense implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idTypeDepense;

	private Timestamp dateEnregTD;

	@Column(length=255)
	private String descriptionTypeDep;

	@Column(length=50)
	private String libelleTypeDepense;

	private double montantPrevu;

	

}