package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the service_a_facturer database table.
 * 
 */
@Entity
@Table(name="service_a_facturer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServiceAFacturer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idLignCMD;

	private Date dateLivCMD;	

	private int facture;
	
	@Column(name="montantht")
	private double montantHT;
	
	@Column(name="montanttva")
	private double montantTVA;	
	
	@Column(name="prix_total")
	private double prixTotal;
	
	@Column(name="prix_unitaire")
	private double prixUnitaire;
	
	@Column(name="montant_total_ttc")
	private double montantTotalTtc;
	
	@Column(name="qttecmd")
	private int qtteCMD;	
	
	@Column(name="tauxtva")
	private double tauxTVA;
	
	@Column(name="tva_externe")
	private double tvaExterne;
	
	@Column(name="libelle_service", length=255)
	private String libelleService;
	
	@Column(name="categorie", length=100)
	private String categorie;

	//bi-directional many-to-one association to Service
	@ManyToOne
	@JoinColumn(name="idService")
	private Service service;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)		
    @JoinColumn(name="idDossier")
	private Dossiers idDossier;

	

}