package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the envoie_email_client database table.
 * 
 */
@Entity
@Table(name="envoie_email_client")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class EnvoieEmailClient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idEnvoie;

	@Lob
	private byte[] contenu;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="`date-email`")
	private Date date_email;

	private BigInteger idDestinataire;

	@Column(length=255)
	private String nomDestinataire;

	@Column(length=255)
	private String objetEmail;

	@Column(length=1024)
	private String urlPieceJointe;

	

}