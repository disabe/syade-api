package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the services database table.
 * 
 */
@Entity
@Table(name="services")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idService;

	private Date dateService;
	
	@Column(length=20)
	private String categorie;

	@Column(length=255)
	private String libelleService;

	@Column(length=100)
	private String materiel;

	private double prixMax;

	private double prixMin;

	@Column(length=100)
	private String referenceServ;

	private double tauxReduction;

	private double tauxTVA;

	//bi-directional many-to-one association to ServiceAFacturer
	@JsonIgnore
	@OneToMany(mappedBy="service")
	private List<ServiceAFacturer> serviceAFacturers;

	
	/*
	 * public ServiceAFacturer addServiceAFacturer(ServiceAFacturer
	 * serviceAFacturer) { getServiceAFacturers().add(serviceAFacturer);
	 * serviceAFacturer.setService(this);
	 * 
	 * return serviceAFacturer; }
	 * 
	 * public ServiceAFacturer removeServiceAFacturer(ServiceAFacturer
	 * serviceAFacturer) { getServiceAFacturers().remove(serviceAFacturer);
	 * serviceAFacturer.setService(null);
	 * 
	 * return serviceAFacturer; }
	 */
}