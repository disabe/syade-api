package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * The persistent class for the entreprise database table.
 * 
 */
@Entity
@Table(name="entreprise")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Entreprise implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long idEntreprise;

	private int activitePrincipal;

	private int bp;

	@Column(name="CDI_CSI", length=20)
	private String cdiCsi;

	@Column(name="CPI_CRI", length=20)
	private String cpiCri;

	@Column(length=100)
	private String email;

	private int indexTelephonique;

	@Column(name="lieu_dit", length=255)
	private String lieuDit;

	@Column(length=100)
	private String numContribuable;

	@Column(length=100)
	private String numRC;

	@Column(length=50)
	private String pays;

	@Column(length=100)
	private String quartier;

	@Column(length=255)
	private String raisonSocial;

	@Column(name="regime_fiscal", length=100)
	private String regimeFiscal;

	@Column(length=50)
	private String region;

	@Column(length=100)
	private String siegeSocial;

	@Column(length=100)
	private String siteWeb;

	@Column(length=20)
	private String telFixe;

	@Column(length=20)
	private String telMobile1;

	@Column(length=20)
	private String telMobile2;

	@Column(length=100)
	private String ville;

	

}