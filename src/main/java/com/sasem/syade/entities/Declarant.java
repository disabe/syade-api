package com.sasem.syade.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the declarant database table.
 * 
 */
@Entity
@Table(name="declarant")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Declarant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_employe", unique=true, nullable=false)
	private Long idEmploye;

	@Column(name="adresse", length=255)
	private String adresse;

	@Column(name="date_naissance", nullable=false)
	private Timestamp dateNaissance;

	@Column(name="matricule", length=20)
	private String matricule;

	@Column(name="nom_employe", length=100)
	private String nomEmploye;
	@Column(name="pourcentage")
	private int pourcentage;

	@Column(name="prenom_employe", length=100)
	private String prenomEmploye;

	@Column(name="qualite", length=50)
	private String qualite;
	
	@Column(name="email_emp", length=50)
	private String	emailEmp;
	
	@Column(name="salaire_fixe")
	private double salaireFixe;
	
	@Column(name="telephone", length=30)
	private String telephone;
	
	@Column(name="username", length=50)
	private String username;
	
	@Column(name="password", length=255)
	private String password;
	
	@Column(name="enabled")
	private boolean enabled;
	
	@JsonIgnore
	@OneToMany(mappedBy="declarant", fetch=FetchType.EAGER)
	private List<Dossiers> dossiers;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	    @JoinTable(
	            name = "user_role",
	            joinColumns = @JoinColumn(name = "idUser"),
	            inverseJoinColumns = @JoinColumn(name = "idRole")
	            )
	private Set<Role> roles = new HashSet<>();

		

}