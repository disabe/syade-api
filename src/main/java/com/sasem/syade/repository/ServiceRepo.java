package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sasem.syade.entities.Service;

@Repository
public interface ServiceRepo extends JpaRepository<Service, Long>{
	Service findByLibelleServiceAndCategorie(String lib, String cat);
}
