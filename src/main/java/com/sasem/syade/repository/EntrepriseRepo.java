package com.sasem.syade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasem.syade.entities.Entreprise;


public interface EntrepriseRepo extends JpaRepository<Entreprise, Long> {

}
