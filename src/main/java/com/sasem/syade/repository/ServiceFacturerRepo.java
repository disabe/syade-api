package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.ServiceAFacturer;

public interface ServiceFacturerRepo extends JpaRepository<ServiceAFacturer, Long> {
	
	List<ServiceAFacturer> findByIdDossier(Dossiers dossier);

}
