package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sasem.syade.entities.Dossiers;
import com.sasem.syade.entities.Marchandises;

public interface MarchandiseRepo extends JpaRepository<Marchandises, Long>{
	@Query(value="Select m from Marchandises m Where m.dossier = ?1")
	List<Marchandises> listerDossiers(Dossiers dossier);
}
