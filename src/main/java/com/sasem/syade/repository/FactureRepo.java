package com.sasem.syade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasem.syade.entities.Facture;


public interface FactureRepo extends JpaRepository<Facture, Long> {

}
