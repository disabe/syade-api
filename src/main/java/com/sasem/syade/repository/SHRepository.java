package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sasem.syade.entities.SystemeHarmonise;


public interface SHRepository extends JpaRepository<SystemeHarmonise, String>{
	
	public List<SystemeHarmonise> findByHsCode(String code);
	public List<SystemeHarmonise> findByTardsc(String libelle);

	@Query(value= "Select hs_code From systeme_harmonise", nativeQuery=true)
	public List<String> findAllCode();
	
	@Query(value= "Select tardsc From systeme_harmonise", nativeQuery=true)
	public List<String> findAllLibelle();
	
	
}
