package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.sasem.syade.entities.Declarant;


public interface DeclarantRepo extends JpaRepository<Declarant, Long>  {

	List<Declarant> findByNomEmploye(String nom);
	List<Declarant> findByPrenomEmploye(String nom);
	Declarant findByUsername(String unsename);
	
	Boolean existsByUsername(String username);

	Boolean existsByEmailEmp(String email);
	
}
