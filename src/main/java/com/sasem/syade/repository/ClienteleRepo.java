package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasem.syade.entities.Clientele;

public interface ClienteleRepo extends JpaRepository<Clientele, Long> {

	List<Clientele> findByRaisonSocialeClient(String raison);

}
