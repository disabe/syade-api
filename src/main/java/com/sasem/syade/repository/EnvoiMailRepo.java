package com.sasem.syade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasem.syade.entities.EnvoieEmailClient;


public interface EnvoiMailRepo extends JpaRepository<EnvoieEmailClient, Long>  {

}
