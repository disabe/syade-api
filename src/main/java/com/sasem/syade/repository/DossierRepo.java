package com.sasem.syade.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sasem.syade.entities.Clientele;
import com.sasem.syade.entities.Declarant;
import com.sasem.syade.entities.Dossiers;

public interface DossierRepo extends JpaRepository<Dossiers, Long> {
	
	List<Dossiers> findByClientele(Clientele client);
	List<Dossiers> findByDeclarant(Declarant declarant);
	
	@Query("SELECT d FROM Dossiers d WHERE d.declarant = null")	
	List<Dossiers> getDossierLibre();
	
	
}
