package com.sasem.syade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sasem.syade.entities.Devises;


public interface DevisesRepo  extends JpaRepository<Devises, Long> {
	

}
