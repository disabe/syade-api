package com.sasem.syade.batch;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;


import com.sasem.syade.entities.SystemeHarmonise;
import com.sasem.syade.repository.SHRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SHWriter implements ItemWriter<SystemeHarmonise> {

	@Autowired
	private SHRepository repo;
	
	 @Override
	    public void write(List<? extends SystemeHarmonise> sh) {
	        sh.stream().forEach(sh1 -> {
	            log.info("Enregistrement en base des elements du systeme harmonise {}", sh1);
	            repo.save(sh1);
	        });
	    }
	
}
