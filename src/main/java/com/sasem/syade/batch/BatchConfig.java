package com.sasem.syade.batch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PushbackInputStream;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.mapping.BeanWrapperRowMapper;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.sasem.syade.batch.SHProcessor;
import com.sasem.syade.batch.SHWriter;
import com.sasem.syade.entities.SystemeHarmonise;
import com.sasem.syade.batch.SHExcelRowMapper;

import lombok.extern.slf4j.Slf4j;

/*@Configuration
@EnableBatchProcessing
@EnableScheduling*/
@Slf4j
public class BatchConfig {

	// private static final String FILE_NAME = "results.csv";
	private static final String JOB_NAME = "listSHJob";
	private static final String STEP_NAME = "processingStep";
	private static final String READER_NAME = "shItemReader";

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	public Step shStep() throws FileNotFoundException {
		return stepBuilderFactory.get(STEP_NAME).<SystemeHarmonise, SystemeHarmonise>chunk(1).reader(excelSHReader())
				.processor(shItemProcessor()).writer(shItemWriter()).build();
	}

	@Bean
	public Job listSHJob(Step step1) {
		return jobBuilderFactory.get(JOB_NAME).start(step1).build();
	}

	@Bean
	ItemReader<SystemeHarmonise> excelSHReader() throws FileNotFoundException {

		
		  PoiItemReader<SystemeHarmonise> reader = new PoiItemReader<>();
		  PushbackInputStream input;
		  
		  input = new PushbackInputStream(new FileInputStream("C:\\Users\\Sabech\\Documents\\eclipse\\syade\\src\\main\\resources\\data\\system_harmonise.xls"));
		  InputStreamResource resource = new InputStreamResource(input);
		  reader.setResource(resource);		  
		  reader.setRowMapper(new SHExcelRowMapper()); 
		  return reader;
		 
		
		/*
		 * PoiItemReader<SystemeHarmonise> reader = new PoiItemReader<>();
		 * reader.setLinesToSkip(1); log.info("entrain de read"); reader.setResource(new
		 * ClassPathResource("data/system_harmonise.xls"));
		 * reader.setRowMapper(excelRowMapper()); log.info("a deja read 2"); return
		 * reader;
		 */
		 
	}

	public RowMapper<SystemeHarmonise> excelRowMapper() {
		BeanWrapperRowMapper<SystemeHarmonise> rowMapper = new BeanWrapperRowMapper<>();
		rowMapper.setTargetType(SystemeHarmonise.class);
		log.info("parser le fichier excel");
		return rowMapper;
	}

	@Bean
	public ItemProcessor<SystemeHarmonise, SystemeHarmonise> shItemProcessor() {
		log.info("processor");
		return new SHProcessor();
	}

	@Bean
	public ItemWriter<SystemeHarmonise> shItemWriter() {
		log.info("writter");
		return new SHWriter();
	}
}
