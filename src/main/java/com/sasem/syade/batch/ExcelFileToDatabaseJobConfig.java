package com.sasem.syade.batch;

import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.mapping.BeanWrapperRowMapper;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.core.io.ClassPathResource;

import com.sasem.syade.entities.SystemeHarmonise;

//@Configuration
public class ExcelFileToDatabaseJobConfig {
	

	 @Bean
	    ItemReader<SystemeHarmonise> excelStudentReader() {
	        PoiItemReader<SystemeHarmonise> reader = new PoiItemReader<>();
	        reader.setLinesToSkip(1);
	        reader.setResource(new ClassPathResource("data/system_harmonise.xlsx"));
	        reader.setRowMapper(excelRowMapper());
	        return reader;
	    }
	    private RowMapper<SystemeHarmonise> excelRowMapper() {
	        BeanWrapperRowMapper<SystemeHarmonise> rowMapper = new BeanWrapperRowMapper<>();
	        rowMapper.setTargetType(SystemeHarmonise.class);
	        return rowMapper;
	    }
	
}
