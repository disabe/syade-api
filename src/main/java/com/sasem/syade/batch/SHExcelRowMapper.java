package com.sasem.syade.batch;

import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

import com.sasem.syade.entities.SystemeHarmonise;

public class SHExcelRowMapper implements RowMapper<SystemeHarmonise> {
	
    @Override
    public SystemeHarmonise mapRow(RowSet rowSet) throws Exception {
    	SystemeHarmonise sys = new SystemeHarmonise();
    	sys.setHsCode(rowSet.getColumnValue(0));
    	sys.setTardsc(rowSet.getColumnValue(1));
    	sys.setDdi(rowSet.getColumnValue(2));
    	sys.setDac(rowSet.getColumnValue(3));
    	sys.setTva(rowSet.getColumnValue(4));
    	sys.setDds(rowSet.getColumnValue(5));
    	sys.setTcv(rowSet.getColumnValue(6));
    	sys.setIsi(rowSet.getColumnValue(7));
    	sys.setIse(rowSet.getColumnValue(8));
    	sys.setTcc(rowSet.getColumnValue(9));
    	sys.setVpf(rowSet.getColumnValue(10));
    	sys.setSte(rowSet.getColumnValue(11));
    	sys.setCodeMercurial(rowSet.getColumnValue(12));
        return sys;
    }
}