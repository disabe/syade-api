package com.sasem.syade.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.sasem.syade.batch.BatchLauncher;

import lombok.extern.slf4j.Slf4j;

//@Component
@Slf4j
public class Scheduler {

    @Autowired
    private BatchLauncher batchLauncher;

    @Scheduled(fixedDelay = 80000)
    public void perform() throws Exception {
        log.info("Batch programmé pour tourner toutes les 80 secondes");
      // batchLauncher.run();
    }
}