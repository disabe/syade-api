package com.sasem.syade.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sasem.syade.entities.Declarant;
import com.sasem.syade.repository.DeclarantRepo;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	DeclarantRepo userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Declarant user = userRepository.findByUsername(username);
				//.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		 if (user == null) {
	            throw new UsernameNotFoundException("Could not find user");
	        }
	         
	        return UserDetailsImpl.build(user);
		
	}
	
}
