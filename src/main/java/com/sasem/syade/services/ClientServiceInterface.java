package com.sasem.syade.services;

import java.util.List;

import com.sasem.syade.entities.Clientele;

public interface ClientServiceInterface {

	public Clientele createClient(Clientele client) throws Exception;
	public List<Clientele> listeClient() throws Exception;
	public Clientele updateClient(Clientele client) throws Exception;
	public void deleteClient(Long idClient) throws Exception;
	
	public Clientele findByRaisonSocialeClient(String raison) throws Exception;
	public Clientele findClientByID(Long idClient) throws Exception;
	
}
