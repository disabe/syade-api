package com.sasem.syade;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.sasem.syade.entities.Declarant;
import com.sasem.syade.payload.DeclarantDTO;
import com.sasem.syade.repository.DeclarantRepo;
import com.sasem.syade.repository.DossierRepo;
import com.sasem.syade.repository.RoleRepo;

//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class GrhControlerTest {

	@Mock 
	private DossierRepo dossierServiceMock;

	@Mock
	private DeclarantRepo declarantRepoMock;
	
	
	private DeclarantDTO declarantDTOMock;
	
	@Mock
	private Declarant declarantMock;
	
	@Mock
	private RoleRepo roleRepoMock;
	
	@Mock
	PasswordEncoder encoderMock;
	
	/*
	 * @BeforeAll public void initMocks() { MockitoAnnotations.initMocks(this); }
	 */
	
	@Test
    void createDeclarantTest(){
		//Given
		declarantDTOMock = new DeclarantDTO();
		declarantDTOMock.setAdresse("Douala");
		declarantDTOMock.setEmailEmp("disabe@yahoo.fr");
		declarantDTOMock.setMatricule("0000");
		declarantDTOMock.setNomEmploye("Disabe");
		declarantDTOMock.setPassword("0000");
		declarantDTOMock.setPrenomEmploye("Didier");
		declarantDTOMock.setQualite("DG");
		declarantDTOMock.setTelephone("927866194");
		declarantDTOMock.setUsername("disabe");
		
		declarantMock = new Declarant();
		declarantMock.setAdresse(declarantDTOMock.getAdresse());
		declarantMock.setEmailEmp(declarantDTOMock.getEmailEmp());
		declarantMock.setMatricule(declarantDTOMock.getMatricule());
		declarantMock.setNomEmploye(declarantDTOMock.getNomEmploye());
		declarantMock.setPassword(declarantDTOMock.getPassword());
		declarantMock.setPrenomEmploye(declarantDTOMock.getPrenomEmploye());
		declarantMock.setQualite(declarantDTOMock.getQualite());
		declarantMock.setTelephone(declarantDTOMock.getTelephone());
		declarantMock.setUsername(declarantDTOMock.getUsername());
		
		//When
		when((((declarantRepoMock.save(declarantMock))))).thenReturn(declarantMock);
		
		//Then
		Declarant savedDeclarant = declarantRepoMock.save(declarantMock);	       
		    assertThat(savedDeclarant.getAdresse()).isEqualTo("Douala");
		    assertThat(savedDeclarant.getEmailEmp()).isEqualTo("disabe@yahoo.fr");
		    assertThat(savedDeclarant.getMatricule()).isEqualTo("0000");
		    assertThat(savedDeclarant.getNomEmploye()).isEqualTo("Disabe");
		
	}
	
	
	
}
