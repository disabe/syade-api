package com.sasem.syade.repository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sasem.syade.entities.Clientele;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class ClienteleRepoTest {
	
	@Autowired
	private ClienteleRepo clienteleRepoMock;
	
	private final String RAISON_SOCIALE = "Didier Sarl";
	
	//@Captor
	//ArgumentCaptor<Clientele> captor;
	
	@Autowired
    private TestEntityManager entityManager;
	
	@Test
	void whenFindByRaisonSociale_thenReturnClient() {
		//Act GIVEN
		final Clientele clientTest = new Clientele();
		clientTest.setRaisonSocialeClient(RAISON_SOCIALE);
		clientTest.setIdClient(10L);
		this.entityManager.persist(clientTest);
        this.entityManager.flush();
		
		//Arrange WHEN
        final Clientele clientBDD = clienteleRepoMock.findByRaisonSocialeClient(RAISON_SOCIALE).get(0);
		//when(clienteleRepoMock.findByRaisonSocialeClient(RAISON_SOCIALE)).thenReturn(liste);
		//verify(clienteleRepoMock, times(1)).findByRaisonSocialeClient(RAISON_SOCIALE);
		//Clientele clientResult = liste.get(0);
		
		
		//Assert   THEN                                                                          
		assertEquals(clientBDD.getRaisonSocialeClient(), RAISON_SOCIALE, "Ok Trouvé !!!");
		assertNotNull(clientBDD.getIdClient());
		assertNotNull(clientBDD);
	}

	@Test
	void testFindAll() {
		//fail("Not yet implemented");
	}

	@Test
	void testGetOne() {
		//fail("Not yet implemented");
	}

	@Test
	void testSave() {
		//fail("Not yet implemented");
	}

	@Test
	void testFindById() {
		//fail("Not yet implemented");
	}

	@Test
	void testDeleteById() {
		//fail("Not yet implemented");
	}

}
